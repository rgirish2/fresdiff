/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2012. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1       /* callback function: panelCB */
#define  PANEL_COMMANDBUTTON              2       /* control type: command, callback function: Start */
#define  PANEL_QUITBUTTON                 3       /* control type: command, callback function: QuitCallback */

#define  PLOT_1                           2
#define  PLOT_1_GRAPH                     2       /* control type: graph, callback function: (none) */
#define  PLOT_1_COMMANDBUTTON             3       /* control type: command, callback function: PRINT */
#define  PLOT_1_OKBUTTON                  4       /* control type: command, callback function: OkCallback */
#define  PLOT_1_COMMANDBUTTON_2           5       /* control type: command, callback function: Save_Data */

#define  PLOT_2                           3
#define  PLOT_2_GRAPH                     2       /* control type: graph, callback function: (none) */
#define  PLOT_2_COMMANDBUTTON             3       /* control type: command, callback function: PRINT */
#define  PLOT_2_OKBUTTON                  4       /* control type: command, callback function: OkCallback */
#define  PLOT_2_COMMANDBUTTON_2           5       /* control type: command, callback function: Save_Data */

#define  PLOT_3                           4
#define  PLOT_3_GRAPH                     2       /* control type: graph, callback function: (none) */
#define  PLOT_3_COMMANDBUTTON             3       /* control type: command, callback function: PRINT */
#define  PLOT_3_OKBUTTON                  4       /* control type: command, callback function: OkCallback */
#define  PLOT_3_COMMANDBUTTON_2           5       /* control type: command, callback function: Save_Data */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK OkCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK panelCB(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK PRINT(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Save_Data(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Start(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
