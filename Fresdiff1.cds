<distribution version="10.0.1" name="Expt D2 Fresnel Simulation" type="MSI">
	<prebuild>
		<workingdir>workspacedir</workingdir>
		<actions></actions></prebuild>
	<postbuild>
		<workingdir>workspacedir</workingdir>
		<actions></actions></postbuild>
	<msi GUID="{3599B238-D614-4ECB-B395-7FA939310921}">
		<general appName="Expt D2 Fresnel Simulation" outputLocation="c:\Users\rgirish2\Desktop\Fresnel_diff\cvidistkit.Expt D2 Fresnel Simulation" relOutputLocation="cvidistkit.Expt D2 Fresnel Simulation" outputLocationWithVars="c:\Users\rgirish2\Desktop\Fresnel_diff\cvidistkit.%name" relOutputLocationWithVars="cvidistkit.%name" upgradeBehavior="1" autoIncrement="true" version="1.0.3">
			<arp company="rgirish2" companyURL="" supportURL="" contact="" phone="" comments="© 2012 University of Illinois Board of Trustees"/>
			<summary title="Expt D2 Fresnel Simulation" subject="Optical Physics Lab" keyWords="Physics" comments="" author="rgirish2"/></general>
		<userinterface language="English" showPaths="true" readMe="" license="c:\Users\rgirish2\Desktop\Fresnel_diff\Primary Release License.rtf" relLicense="Primary Release License.rtf">
			<dlgstrings welcomeTitle="Expt D2 Fresnel Simulation" welcomeText=""/></userinterface>
		<dirs appDirID="100">
			<installDir name="[Program Files]" dirID="2" parentID="-1" isMSIDir="true" visible="true" unlock="false"/>
			<installDir name="Expt D2 Fresnel Simulation" dirID="100" parentID="2" isMSIDir="false" visible="true" unlock="false"/>
			<installDir name="[Start&gt;&gt;Programs]" dirID="7" parentID="-1" isMSIDir="true" visible="true" unlock="false"/>
			<installDir name="Expt D2 Fresnel Simulation" dirID="101" parentID="7" isMSIDir="false" visible="true" unlock="false"/></dirs>
		<files>
			<simpleFile fileID="0" sourcePath="c:\Users\rgirish2\Desktop\Fresnel_diff\Fresdiff1.uir" relSourcePath="Fresdiff1.uir" relSourceBase="0" targetDir="100" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/>
			<simpleFile fileID="1" sourcePath="c:\Users\rgirish2\Desktop\Fresnel_diff\cvibuild.Fresdiff1\Release\Fresdiff1.exe" targetDir="100" readonly="false" hidden="false" system="false" regActiveX="false" runAfterInstallStyle="IMMEDIATELY_RESUME_INSTALL" cmdLineArgs="" runAfterInstall="false" uninstCmdLnArgs="" runUninst="false"/></files>
		<fileGroups>
			<projectOutput targetType="0" dirID="100" projectID="0">
				<fileID>1</fileID></projectOutput>
			<projectDependencies dirID="100" projectID="0"/></fileGroups>
		<shortcuts>
			<shortcut name="Fresdiff1" targetFileID="1" destDirID="101" cmdLineArgs="" description="" runStyle="NORMAL"/></shortcuts>
		<mergemodules/>
		<products/>
		<runtimeEngine cvirte="true" instrsup="true" lvrt="true" analysis="true" netvarsup="true" dotnetsup="true" activeXsup="true" lowlevelsup="true" rtutilsup="true" installToAppDir="false"/>
		<advanced mediaSize="650">
			<launchConditions>
				<condition>MINOS_WINXP</condition>
			</launchConditions>
			<includeConfigProducts>true</includeConfigProducts>
			<maxImportVisible>silent</maxImportVisible>
			<maxImportMode>merge</maxImportMode>
			<custMsgFlag>false</custMsgFlag>
			<custMsgPath>c:\program files\national instruments\cvi2010\bin\msgrte.txt</custMsgPath>
			<signExe>false</signExe>
			<certificate></certificate>
			<signTimeURL></signTimeURL>
			<signDescURL></signDescURL></advanced>
		<Projects NumProjects="1">
			<Project000 ProjectID="0" ProjectAbsolutePath="c:\Users\rgirish2\Desktop\Fresnel_diff\Fresdiff1.prj" ProjectRelativePath="Fresdiff1.prj"/></Projects>
		<buildData progressBarRate="2.421068769913669">
			<progressTimes>
				<Begin>0.000000000000000</Begin>
				<ProductsAdded>0.090961375000000</ProductsAdded>
				<DPConfigured>1.097209750000000</DPConfigured>
				<DPMergeModulesAdded>3.357210875000000</DPMergeModulesAdded>
				<DPClosed>5.938462625000000</DPClosed>
				<DistributionsCopied>8.338462750000000</DistributionsCopied>
				<End>41.304072500000004</End></progressTimes></buildData>
	</msi>
</distribution>
