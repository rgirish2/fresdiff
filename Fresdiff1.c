/*============================================================*/
/*                         Fresdiff1.c                        */
/*============================================================*/
/*                                                            */
/* Fresnel Diffraction program for UIUC Physics 402           */
/* 															  */
/* Undergraduate Light Lab Course                             */
/* 															  */
/* Written by Rishi Girish(rgirish2), Summer 2012             */
/*============================================================*/

#include <Windows.h>
#include <Lmcons.h>
#include <utility.h>
#include <formatio.h>
#include <ansi_c.h>
#include <math.h>
#include <analysis.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "Fresdiff1.h"

int panel_handle;
int panel_handle_1;
int panel_handle_2;
int panel_handle_3;

int times = 0;

char choice[11];
 
int inc = 0;

int a = 0;

int u = 0;

char *curr_date [10];
char *curr_time  [8];

double w;
double lambda;
double lambda1;
double r = 1;
double rho = 1;
double intensity = 1e-3;
char inw[11];
char inwave[11];
char inrho[11];
char inr[11];
char in[11];

double temp1[1000000]={0};
double temp2[1000000]={0};
double tempx1[1000000]={0};
double tempx2[1000000]={0};

double fresnelSineIntegral[1] = {0};
double fresnelCosineIntegral[1] = {0};

double iutemp[1000000]={0};
double fresc[1]={0};
double fress[1]={0};

double cutemp[1000000]={0};
double sutemp[1000000]={0};

double xiutemp[1000000]={0};
double xfresnelSineIntegral[1] = {0};
double xfresnelCosineIntegral[1] = {0};
double xyfresnelSineIntegral[1] = {0};
double xyfresnelCosineIntegral[1] = {0};

/*===============================================================================================================================*/

main ()
{
    panel_handle = LoadPanel(0,"Fresdiff1.uir", PANEL);
	
	panel_handle_1 = LoadPanel(panel_handle, "Fresdiff1.uir", PLOT_1);
	
	panel_handle_2 = LoadPanel(panel_handle, "Fresdiff1.uir", PLOT_2);
	
	panel_handle_3 = LoadPanel(panel_handle, "Fresdiff1.uir", PLOT_3);
	
	DisplayPanel (panel_handle);
	
	MessagePopup("Fresnel Diffraction Graphs", " For HELP, right-click on any button of the application."); 
	
	RunUserInterface ();
	
}

/*===============================================================================================================================*/

void calc ()
{
	double e = -10;
	for(int d = 0; d <=400; d++)
	{
		FresnelIntegrals (e,fresnelSineIntegral,fresnelCosineIntegral);
		sutemp[d] = fresnelSineIntegral[0];
		cutemp[d] = fresnelCosineIntegral[0];
		e = e + 0.05;
	}
}

/*===============================================================================================================================*/ 

void calc_2 (double x)
{
	double test = x;
	test= -(test/2);
	while (test<=x/2 )
	{
		tempx1[a] = test;
		temp1[a] = tempx1[a]*(sqrt(2/(lambda*r)));
		FresnelIntegrals (temp1[a], fress, fresc);
		double in1 = 0.5-fresc[0];
		double in2 = 0.5-fress[0];
		iutemp[a] = ((intensity/2)*((in1*in1)+(in2*in2)));
		test = test + 0.000005;
		a++;
	}
	return;	
}

/*===============================================================================================================================*/

void calc_3 (double x)
{
	double xtest = 0.1;
	xtest= -(xtest/2);
	double delv = (x)*(sqrt(2/(lambda*r))); 
	while (xtest<=0.05 )
	{
		tempx2[u] = xtest;
		temp2[u] = tempx2[u]*(sqrt(2/(lambda*r)));
		double xf = temp2[u] - delv;
		double xyf = temp2[u] + delv;
		FresnelIntegrals (xf,xfresnelSineIntegral,xfresnelCosineIntegral);
		FresnelIntegrals (xyf,xyfresnelSineIntegral,xyfresnelCosineIntegral);
		double inx1 = 1 + xfresnelCosineIntegral[0] - xyfresnelCosineIntegral[0];
		double inx2 = 1 + xfresnelSineIntegral[0] - xyfresnelSineIntegral[0]; 
		xiutemp[u] = ((0.5)*(pow(inx1,2) + pow(inx2,2)));
		xiutemp[u] = (xiutemp[u] / intensity);
		xtest = xtest + 0.000005;
		u++;
	}
	return;
}
	
/*===============================================================================================================================*/ 

int CVICALLBACK Start(int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
	
	if (event == EVENT_RIGHT_CLICK)
	{
		Beep();
		MessagePopup (" Fresenel Diffraction Graphs ",
                      " START Button Help -- Click this button to start the application. ");
	}

	if (event == EVENT_LEFT_CLICK && times ==0) 
	{
	
		times = 1;
		
		PromptPopup ("Fresnel Diffraction Graphs", "Please select type of obstacle.\n 1 = SEMI-INFINITE PLANE obstacle\n 2 = THIN obstacle", choice, 10);
		
		sscanf( choice, "%d", &inc);
		
		while ( (inc < 1) || (inc > 2))
		{
			MessagePopup("Fresnel Diffraction Graphs", "Invalid value entered");
				
			PromptPopup ("Fresnel Diffraction Graphs", "Please select type of obstacle.\n 1 = SEMI-INFINITE PLANE obstacle\n 2 = THIN obstacle", choice, 10);
			
			sscanf( choice, "%d", &inc);
		}
		
	    PromptPopup ("Fresnel Diffraction Graphs",
		        "Please enter the WIDTH/THICKNESS of the slit/obstacle in m.\ne.g. 0.005\nRange (0 -- 0.01m)." , inw, 10);
		
    	w = strtod(inw,NULL);
		 
		while (w<0 || w>0.01 )
		{
			MessagePopup("Fresnel Diffraction Graphs", "Invalid value entered");
			
			PromptPopup ("Fresnel Diffraction Graphs",
		        "Please enter the WIDTH/THICKNESS of the slit/obstacle in m.\ne.g. 0.005\nRange (0 -- 0.01m)." , inw, 10);
		
    	    w = strtod(inw,NULL);
		}
		

		PromptPopup ("Fresnel Diffraction Graphs",
		        "Please enter the WAVELENGTH of source used in nm.\ne.g. 632.85\nRange (100nm -- 1000nm)." , inwave, 10);
		
		lambda1 = strtod(inwave,NULL);
		lambda = lambda1*1e-9;
		
		while (lambda1<(100) || lambda1>(1000) )
		{
			MessagePopup("Fresnel Diffraction Graphs", "Invalid value entered");
			  
			PromptPopup ("Fresnel Diffraction Graphs",
		        "Please enter the WAVELENGTH of source used in nm.\ne.g. 632.85\nRange (100nm -- 1000nm)." , inwave, 10);
		
		    lambda1 = atof(inwave);
			lambda = lambda1*1e-9;
		}

		PromptPopup ("Fresnel Diffraction Graphs",
		        "Please enter the distance between source and obstacle in m.\ne.g. 2.5\nRange (0m -- 10m). " , inrho, 10);
		
		rho = strtod(inrho, NULL);
		
		while(rho<0 || rho>10 )
		{
		
			MessagePopup("Fresnel Diffraction Graphs", "Invalid value entered");
			
			PromptPopup ("Fresnel Diffraction Graphs",
		        "Please enter the distance between source and obstacle in m.\ne.g. 2.5\nRange (0m -- 10m). " , inrho, 10);
		
		    rho = strtod(inrho, NULL);
			
		}
			
		
		PromptPopup ("Fresnel Diffraction Graphs",
		        "Please enter the distance between obstacle and detector in m. \ne.g. 2.5\nRange (0m -- 10m). " , inr, 10);
		
		r = strtod(inr, NULL);
		
		while(r<0 || r>10 )
		{
		 
			MessagePopup("Fresnel Diffraction Graphs", "Invalid value entered");
			
			PromptPopup ("Fresnel Diffraction Graphs",
		        "Please enter the distance between obstacle and detector in m. \ne.g. 2.5\nRange (0m -- 10m). " , inr, 10);
		
		    r = strtod(inr, NULL);
			
		}
			
		PromptPopup ("Fresnel Diffraction Graphs",
		        "Please enter the INTENSITY at the souce in mW. \ne.g. 10\nRange(0mW -- 100mW)" , in, 10);
		
		intensity = strtod(in, NULL);
		
		while(intensity<0 || intensity>100 )
		{
			
			MessagePopup("Fresnel Diffraction Graphs", "Invalid value entered");
			
			PromptPopup ("Fresnel Diffraction Graphs",
		        "Please enter the INTENSITY at the souce in mW. \ne.g. 10\nRange(0mW -- 100mW)" , in, 10);
		
		    intensity = strtod(in, NULL);
			
		}
		
		calc();
		
	    calc_2(w);
		
		calc_3(w);
		
		DisplayPanel(panel_handle_1);

	/*	XYGraphPopup( "C(u) vs. S(u)", cutemp, sutemp, 200, VAL_DOUBLE, VAL_DOUBLE);*/
		PlotXY ( panel_handle_1, PLOT_1_GRAPH, cutemp, sutemp, 400, VAL_DOUBLE, VAL_DOUBLE, VAL_THIN_LINE, VAL_NO_POINT, VAL_SOLID, 1, VAL_DK_BLUE);
		
		Sleep(1000);
			
		if ( inc == 1)
		{
			DisplayPanel(panel_handle_2);
	    	/*XYGraphPopup( "SEMI-INFINITE PLANE Obstacle Graph", tempx, iutemp, count, VAL_DOUBLE, VAL_DOUBLE); */
			PlotXY ( panel_handle_2, PLOT_2_GRAPH, tempx1, iutemp, a, VAL_DOUBLE, VAL_DOUBLE, VAL_THIN_LINE, VAL_NO_POINT, VAL_SOLID, 1, VAL_DK_BLUE);
		}
		else
		{	
			DisplayPanel(panel_handle_3);
			/*XYGraphPopup( "THIN Obstacle Graph", tempx, xiutemp, count, VAL_DOUBLE, VAL_DOUBLE); */
			PlotXY ( panel_handle_3, PLOT_3_GRAPH, tempx2, xiutemp, u, VAL_DOUBLE, VAL_DOUBLE, VAL_THIN_LINE, VAL_NO_POINT, VAL_SOLID, 1, VAL_DK_BLUE);
		}
		
		Sleep(1000);
		
		MessagePopup( "Fresnel Diffraction Graphs", "This Program runs only once with the entered values.\nFor a new set of values, please restart the program.");
	}
	
	return (0);
}

/*===============================================================================================================================*/
	
int CVICALLBACK QuitCallback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
	switch (event == EVENT_LEFT_CLICK)
		{
		case EVENT_COMMIT:
			QuitUserInterface (0);
			break;
		}
	switch (event == EVENT_RIGHT_CLICK)
	{
		case EVENT_COMMIT:
		     Beep();
	       	 MessagePopup (" Fresenel Diffraction Graphs ",
                       " QUIT Button Help -- This button exits from the program.");
	}
	return 0;
}

/*===============================================================================================================================*/
void save()
{
	int save_cancel;
	int the_file;
	
	TCHAR name[ UNLEN + 1 ];
    DWORD size = UNLEN + 1;

	char   my_path  [260];

	save_cancel = FileSelectPopup ("C:\Users\Public\Data", "*.txt", "*.txt",
				 "Save the Data Where?", VAL_SAVE_BUTTON, 0, 0, 1, 1, my_path);

	if (save_cancel != 0)
	{

		the_file = OpenFile (my_path, 2, 0, 1);

		FmtFile   (the_file, "%s\n",my_path);
		
    	WriteFile (the_file, "\nFresnel Diffraction \n\n", 24);
		
		FmtFile (the_file, "%s","Date: ");
		WriteFile (the_file, *curr_date, 10);

		FmtFile (the_file, "%s", "\tTime: ");
		WriteFile (the_file, *curr_time,  8);
		
		GetUserName( (TCHAR*)name, &size );
		FmtFile( the_file, "%s%s", "\n\nUsername: ", name);	
		
		FmtFile (the_file, "%s", "\n\nCORNU Spiral Data Set\n\nParameters \n");
		
		FmtFile (the_file, "%s%f", " Width (m):\t",w);
		FmtFile (the_file, "%s%f", "\n Wavelength (m):\t",lambda);
		FmtFile (the_file, "%s%f", "\n Source-Obstacle Distance (m):\t",rho);
		FmtFile (the_file, "%s%f", "\n Obstacle-Screen Distance (m):\t",r);
		FmtFile (the_file, "%s%f", "\n Intensity (mW):\t",intensity);
	
	
		WriteFile (the_file, "\n\n\t\tS(u)\t\tC(u)\n", 15);
		
	    for(int m =0; m<=400; m++)
       	{
			   FmtFile(the_file,"%s%f[w15]%s%f[w15]%s","\t", sutemp[m],"\t",cutemp[m],"\n");
			   m++; 
		}
	}
}

/*===============================================================================================================================*/

void save_2()
{
	int m = 0;
	int save_cancel;
	int the_file;
	
	TCHAR name[ UNLEN + 1 ];
    DWORD size = UNLEN + 1;

	char   my_path  [260];

	save_cancel = FileSelectPopup ("C:\Users\Public\Data", "*.txt", "*.txt",
				 "Save the Data Where?", VAL_SAVE_BUTTON, 0, 0, 1, 1, my_path);

	if (save_cancel != 0)
	{

		the_file = OpenFile (my_path, 2, 0, 1);

		FmtFile   (the_file, "%s\n",my_path);
		
		WriteFile (the_file, "\nFresnel Diffraction \n\n", 24);
		
		FmtFile (the_file, "%s","Date: ");
		WriteFile (the_file, *curr_date, 10);

		FmtFile (the_file, "%s", "\tTime: ");
		WriteFile (the_file, *curr_time,  8);
		
		GetUserName( (TCHAR*)name, &size );
		FmtFile( the_file, "%s%s", "\n\nUsername: ", name);
		
		FmtFile (the_file, "%s", "\n\nSEMI-Infinite Plane Obstacle Diffraction \n\nParameters \n");
		
		FmtFile (the_file, "%s%f", " Width (m):\t",w);
		FmtFile (the_file, "%s%f", "\n Wavelength (m):\t",lambda);
		FmtFile (the_file, "%s%f", "\n Source-Obstacle Distance (m):\t",rho);
		FmtFile (the_file, "%s%f", "\n Obstacle-Screen Distance (m):\t",r);
		FmtFile (the_file, "%s%f", "\n Intensity (mW):\t",intensity);
		
		WriteLine (the_file, "\n\n\t\tX(m)\t\tI(x)\n", 15);
		
		while(m<=a)
		{
			FmtFile (the_file,"%s%f[w15]%s%f[w15]%s","\t", tempx1[m],"\t",iutemp[m],"\n");
			m++;
		}
		
	}
}

/*===============================================================================================================================*/

void save_3()
{
	int m = 0;
	int save_cancel;
	int the_file;
	
	TCHAR name[ UNLEN + 1 ];
    DWORD size = UNLEN + 1;

	char   my_path  [260];

	save_cancel = FileSelectPopup ("C:\Users\Public\Data", "*.txt", "*.txt",
				 "Save the Data Where?", VAL_SAVE_BUTTON, 0, 0, 1, 1, my_path);

	if (save_cancel != 0)
	{

		the_file = OpenFile (my_path, 2, 0, 1);

		FmtFile   (the_file, "%s\n",my_path);
		
		WriteFile (the_file, "\nFresnel Diffraction \n\n", 24);
		
		FmtFile (the_file, "%s","Date: ");
		WriteFile (the_file, *curr_date, 10);

		FmtFile (the_file, "%s", "\tTime: ");
		WriteFile (the_file, *curr_time,  8);
		
		GetUserName( (TCHAR*)name, &size );
		FmtFile( the_file, "%s%s", "\n\nUsername: ", name);
		
		FmtFile (the_file, "%s" ,"\n\nTHIN Obstacle Diffraction \n\nParameters: \n");
		
		FmtFile (the_file, "%s%f", " Width (m):\t",w);
		FmtFile (the_file, "%s%f", "\n Wavelength (m):\t",lambda);
		FmtFile (the_file, "%s%f", "\n Source-Obstacle Distance (m):\t",rho);
		FmtFile (the_file, "%s%f", "\n Obstacle-Screen Distance (m):\t",r);
		FmtFile (the_file, "%s%f", "\n Intensity (mW):\t",intensity);
		
		WriteLine (the_file, "\n\n\t\tX(m)\t\tRelative I(x)\n", 23);
		
		while(m<=u)
		{
			FmtFile (the_file,"%s%f[w15]%s%f[w15]%s","\t",tempx2[m],"\t",xiutemp[m],"\n");
			m++;
		}	
	}
}

/*===============================================================================================================================*/

int CVICALLBACK panelCB (int panel, int event, void *callbackData, int eventData1, int eventData2)
{
    if (event == EVENT_CLOSE)
        QuitUserInterface (0);
    return 0;
}

/*===============================================================================================================================*/

int  CVICALLBACK PRINT(int panel, int control, int event, void *callbackData, int eventData1, int eventData2) 
{

	if (event == EVENT_RIGHT_CLICK) {
	    Beep();
	    MessagePopup (" PRINT Button Help ",
      				  " This button makes hardcopy of plot and sends file to laser printer. ");
	}

	if (event == EVENT_LEFT_CLICK) 
	{

		int confirm = GetActivePanel();
		
		int set = SetPrintAttribute(ATTR_PRINT_AREA_WIDTH, VAL_INTEGRAL_SCALE);
		
		int set1 = SetPrintAttribute(ATTR_PRINT_AREA_HEIGHT, VAL_USE_ENTIRE_PAPER);
	    
		int check = PrintPanel(confirm, "",1, VAL_FULL_PANEL,0);
	
	    if(check<0)
		MessagePopup (" Fresnel Diffraction ", " Error in printing graph. Please Retry. ");
    	else
		MessagePopup (" Fresnel Diffraction ", " Print Successful ");

	}
	return (0);
}	

/*===============================================================================================================================*/

int  CVICALLBACK OkCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
    if (event == EVENT_LEFT_CLICK)  
	{ 
		int confirm = GetActivePanel();
		HidePanel(confirm);
	}
	
	if (event == EVENT_RIGHT_CLICK)
	{
		MessagePopup("Fresnel Diffraction Graphs", "OK Button Help -- Click this button to hide the present dialog box.");
	}
	return 0;
}

/*===============================================================================================================================*/

int  CVICALLBACK Save_Data(int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
   if (event == EVENT_LEFT_CLICK)
   {
		   *curr_time = TimeStr();
		   *curr_date = DateStr();
		   
		   int confirm = GetActivePanel(); 
		   
		   if(confirm == panel_handle_1)
			   save();
		   else if (confirm == panel_handle_2)
			   save_2();
		   else
			   save_3();
	}
   
   if(event == EVENT_RIGHT_CLICK)
	{
		     Beep();
	       	 MessagePopup (" Fresenel Diffraction Graphs ",
                       " SAVE Button Help -- This button saves computed data to a user selected file.");
	}
	
   return 0;
}

/*===============================================================================================================================*/
